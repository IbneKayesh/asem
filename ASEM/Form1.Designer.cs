﻿namespace ASEM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnNewMail = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.txtAttachment3 = new System.Windows.Forms.TextBox();
            this.txtAttachment2 = new System.Windows.Forms.TextBox();
            this.txtMailBody = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAttachment1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.txtCC = new System.Windows.Forms.TextBox();
            this.txtTO = new System.Windows.Forms.TextBox();
            this.btnSendeMail = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.chkCertificate = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtSenderAddress = new System.Windows.Forms.TextBox();
            this.txtMailServer = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProxyPassword = new System.Windows.Forms.TextBox();
            this.txtProxyUser = new System.Windows.Forms.TextBox();
            this.txtProxyPort = new System.Windows.Forms.TextBox();
            this.txtProxyServer = new System.Windows.Forms.TextBox();
            this.chkDefaulProxy = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(426, 322);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnNewMail);
            this.tabPage1.Controls.Add(this.lblMsg);
            this.tabPage1.Controls.Add(this.txtAttachment3);
            this.tabPage1.Controls.Add(this.txtAttachment2);
            this.tabPage1.Controls.Add(this.txtMailBody);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtAttachment1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtSubject);
            this.tabPage1.Controls.Add(this.txtCC);
            this.tabPage1.Controls.Add(this.txtTO);
            this.tabPage1.Controls.Add(this.btnSendeMail);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(418, 296);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Mail writer";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnNewMail
            // 
            this.btnNewMail.Location = new System.Drawing.Point(364, 64);
            this.btnNewMail.Name = "btnNewMail";
            this.btnNewMail.Size = new System.Drawing.Size(45, 23);
            this.btnNewMail.TabIndex = 21;
            this.btnNewMail.Text = "New";
            this.btnNewMail.UseVisualStyleBackColor = true;
            this.btnNewMail.Click += new System.EventHandler(this.btnNewMail_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.ForeColor = System.Drawing.Color.Red;
            this.lblMsg.Location = new System.Drawing.Point(57, 117);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(0, 13);
            this.lblMsg.TabIndex = 20;
            this.lblMsg.Click += new System.EventHandler(this.lblMsg_Click);
            // 
            // txtAttachment3
            // 
            this.txtAttachment3.Location = new System.Drawing.Point(249, 95);
            this.txtAttachment3.Name = "txtAttachment3";
            this.txtAttachment3.Size = new System.Drawing.Size(82, 20);
            this.txtAttachment3.TabIndex = 19;
            this.txtAttachment3.Click += new System.EventHandler(this.txtAttachment3_Click);
            // 
            // txtAttachment2
            // 
            this.txtAttachment2.Location = new System.Drawing.Point(153, 95);
            this.txtAttachment2.Name = "txtAttachment2";
            this.txtAttachment2.Size = new System.Drawing.Size(90, 20);
            this.txtAttachment2.TabIndex = 18;
            this.txtAttachment2.Click += new System.EventHandler(this.txtAttachment2_Click);
            // 
            // txtMailBody
            // 
            this.txtMailBody.Location = new System.Drawing.Point(9, 133);
            this.txtMailBody.Multiline = true;
            this.txtMailBody.Name = "txtMailBody";
            this.txtMailBody.Size = new System.Drawing.Size(400, 157);
            this.txtMailBody.TabIndex = 3;
            this.txtMailBody.Text = "This is a system generated email, do not reply to this email id.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Attach:";
            // 
            // txtAttachment1
            // 
            this.txtAttachment1.Location = new System.Drawing.Point(57, 95);
            this.txtAttachment1.Name = "txtAttachment1";
            this.txtAttachment1.Size = new System.Drawing.Size(90, 20);
            this.txtAttachment1.TabIndex = 16;
            this.txtAttachment1.Click += new System.EventHandler(this.txtAttachment1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Subject:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "CC:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "TO:";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(57, 66);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(305, 20);
            this.txtSubject.TabIndex = 2;
            this.txtSubject.Text = "Greetings mail from ASEM";
            // 
            // txtCC
            // 
            this.txtCC.Location = new System.Drawing.Point(57, 37);
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(352, 20);
            this.txtCC.TabIndex = 10;
            // 
            // txtTO
            // 
            this.txtTO.Location = new System.Drawing.Point(57, 9);
            this.txtTO.Name = "txtTO";
            this.txtTO.Size = new System.Drawing.Size(352, 20);
            this.txtTO.TabIndex = 1;
            // 
            // btnSendeMail
            // 
            this.btnSendeMail.BackColor = System.Drawing.Color.Transparent;
            this.btnSendeMail.Location = new System.Drawing.Point(335, 93);
            this.btnSendeMail.Name = "btnSendeMail";
            this.btnSendeMail.Size = new System.Drawing.Size(74, 23);
            this.btnSendeMail.TabIndex = 0;
            this.btnSendeMail.Text = "Send Mail";
            this.btnSendeMail.UseVisualStyleBackColor = false;
            this.btnSendeMail.Click += new System.EventHandler(this.btnSendeMail_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chkDefaulProxy);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.txtPort);
            this.tabPage2.Controls.Add(this.chkCertificate);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.txtPassword);
            this.tabPage2.Controls.Add(this.txtSenderAddress);
            this.tabPage2.Controls.Add(this.txtMailServer);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(418, 296);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Account";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtProxyPassword);
            this.panel1.Controls.Add(this.txtProxyUser);
            this.panel1.Controls.Add(this.txtProxyPort);
            this.panel1.Controls.Add(this.txtProxyServer);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(9, 163);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(334, 57);
            this.panel1.TabIndex = 16;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(308, 13);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(35, 20);
            this.txtPort.TabIndex = 7;
            this.txtPort.Text = "587";
            // 
            // chkCertificate
            // 
            this.chkCertificate.AutoSize = true;
            this.chkCertificate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chkCertificate.Location = new System.Drawing.Point(99, 116);
            this.chkCertificate.Name = "chkCertificate";
            this.chkCertificate.Size = new System.Drawing.Size(159, 17);
            this.chkCertificate.TabIndex = 6;
            this.chkCertificate.Text = "Disable Certificate validation";
            this.chkCertificate.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Account:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Outgoing Server:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(99, 80);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(244, 20);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtSenderAddress
            // 
            this.txtSenderAddress.Location = new System.Drawing.Point(99, 46);
            this.txtSenderAddress.Name = "txtSenderAddress";
            this.txtSenderAddress.Size = new System.Drawing.Size(244, 20);
            this.txtSenderAddress.TabIndex = 1;
            this.txtSenderAddress.Text = "ibnekayesh91@outlook.com";
            // 
            // txtMailServer
            // 
            this.txtMailServer.Location = new System.Drawing.Point(99, 13);
            this.txtMailServer.Name = "txtMailServer";
            this.txtMailServer.Size = new System.Drawing.Size(203, 20);
            this.txtMailServer.TabIndex = 0;
            this.txtMailServer.Text = "smtp.office365.com";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(182, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Port:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(182, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Password:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "User:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Server:";
            // 
            // txtProxyPassword
            // 
            this.txtProxyPassword.Location = new System.Drawing.Point(241, 32);
            this.txtProxyPassword.Name = "txtProxyPassword";
            this.txtProxyPassword.Size = new System.Drawing.Size(79, 20);
            this.txtProxyPassword.TabIndex = 19;
            this.txtProxyPassword.UseSystemPasswordChar = true;
            // 
            // txtProxyUser
            // 
            this.txtProxyUser.Location = new System.Drawing.Point(76, 32);
            this.txtProxyUser.Name = "txtProxyUser";
            this.txtProxyUser.Size = new System.Drawing.Size(100, 20);
            this.txtProxyUser.TabIndex = 18;
            // 
            // txtProxyPort
            // 
            this.txtProxyPort.Location = new System.Drawing.Point(241, 2);
            this.txtProxyPort.Name = "txtProxyPort";
            this.txtProxyPort.Size = new System.Drawing.Size(79, 20);
            this.txtProxyPort.TabIndex = 17;
            // 
            // txtProxyServer
            // 
            this.txtProxyServer.Location = new System.Drawing.Point(76, 3);
            this.txtProxyServer.Name = "txtProxyServer";
            this.txtProxyServer.Size = new System.Drawing.Size(100, 20);
            this.txtProxyServer.TabIndex = 16;
            // 
            // chkDefaulProxy
            // 
            this.chkDefaulProxy.AutoSize = true;
            this.chkDefaulProxy.Checked = true;
            this.chkDefaulProxy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDefaulProxy.Location = new System.Drawing.Point(99, 140);
            this.chkDefaulProxy.Name = "chkDefaulProxy";
            this.chkDefaulProxy.Size = new System.Drawing.Size(147, 17);
            this.chkDefaulProxy.TabIndex = 17;
            this.chkDefaulProxy.Text = "Use system proxy settings";
            this.chkDefaulProxy.UseVisualStyleBackColor = true;
            this.chkDefaulProxy.CheckedChanged += new System.EventHandler(this.chkDefaulProxy_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 329);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Sender [Auto Send E-Mail]";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.TextBox txtCC;
        private System.Windows.Forms.Button btnSendeMail;
        private System.Windows.Forms.TextBox txtAttachment1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtMailServer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkCertificate;
        private System.Windows.Forms.TextBox txtMailBody;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtSenderAddress;
        private System.Windows.Forms.TextBox txtAttachment3;
        private System.Windows.Forms.TextBox txtAttachment2;
        private System.Windows.Forms.TextBox txtTO;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnNewMail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProxyPassword;
        private System.Windows.Forms.TextBox txtProxyUser;
        private System.Windows.Forms.TextBox txtProxyPort;
        private System.Windows.Forms.TextBox txtProxyServer;
        private System.Windows.Forms.CheckBox chkDefaulProxy;
    }
}

