﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEM
{
    public partial class Form1 : Form
    {
        string ProxyIP, ProxyUser, ProxyPassword;
        int ProxyPort;
        public Form1()
        {
            InitializeComponent();
        }
        private void btnSendeMailxxx_Click(object sender, EventArgs e)
        {
            string ServerID = txtMailServer.Text.Trim();
            int ServerPort = Convert.ToInt32(txtPort.Text);

            string SenderID = txtSenderAddress.Text;
            string SenderIDAccount = txtSenderAddress.Text;
            string SenderCredntial = txtPassword.Text;

            string TOAddress = txtTO.Text;
            string MailSubject = txtSubject.Text;
            string MailBody = txtMailBody.Text;
            if (txtMailServer.Text == "" || txtPort.Text == "" || txtSenderAddress.Text == "" || txtPassword.Text == "" || txtTO.Text == "" || txtSubject.Text == "" || txtMailBody.Text == "")
            {
                MessageBox.Show("Mail send failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                MailMessage mail = new MailMessage();
                //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                SmtpClient SmtpServer = new SmtpClient(ServerID);
                mail.From = new MailAddress(SenderID);
                mail.To.Add(TOAddress);
                mail.Subject = MailSubject;
                mail.Body = MailBody;
                SmtpServer.Port = ServerPort;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(SenderIDAccount, SenderCredntial);
                SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //SecureSocketOptions.StartTls
                //     SmtpServer.SecureSocketOptions.StartTls;
                SmtpServer.EnableSsl = true;
                if (chkCertificate.Checked == true)
                {
                    MessageBox.Show("Try to send without certificate validation", "ASEM");
                    NEVER_EAT_POISON_Disable_CertificateValidation();
                }
                //  ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                SmtpServer.Send(mail);
                MessageBox.Show("Mail Sent...", "ASEM");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        //    [Obsolete("Do not use this in Production code!!!", true)]
        static void NEVER_EAT_POISON_Disable_CertificateValidation()
        {
            // Disabling certificate validation can expose you to a man-in-the-middle attack
            // which may allow your encrypted message to be read by an attacker
            // https://stackoverflow.com/a/14907718/740639
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                )
                {
                    return true;
                };
        }
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            else
            {
                if (System.Windows.Forms.MessageBox.Show("The server certificate is not valid.\nAccept?", "Certificate Validation", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    return true;
                else
                    return false;
            }
        }
        private void btnSendeMail_Click(object sender, EventArgs e)
        {
            //https://stackoverflow.com/questions/30342884/5-7-57-smtp-client-was-not-authenticated-to-send-anonymous-mail-during-mail-fr/47834615
            string ServerID = txtMailServer.Text.Trim();
            int ServerPort = Convert.ToInt32(txtPort.Text);

            string SenderID = txtSenderAddress.Text;
            string SenderIDAccount = txtSenderAddress.Text.Trim();
            string SenderCredntial = txtPassword.Text.Trim();

            string TOAddress = txtTO.Text;
            string CcAddress = txtCC.Text;
            string MailSubject = txtSubject.Text;
            string MailBody = txtMailBody.Text;
            if (txtMailServer.Text == "" || txtPort.Text == "" || txtSenderAddress.Text == "" || txtPassword.Text == "" || txtTO.Text == "" || txtSubject.Text == "" || txtMailBody.Text == "")
            {
                //MessageBox.Show("", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lblMsg.Text = "Mail send failed... [OK]";
                return;
            }
            // string eml = ("mis95@mis.prangroup.com");
            //var fromAddress = "ibnekayesh91@outlook.com";
            var fromAddress = SenderIDAccount;
            //var toAddress = eml;
            //const string fromPassword = "akhi8240jony";
            string fromPassword = SenderCredntial;
            //  string subject = "test..";
            //   string body = "Welcome..";
            // smtp settings
            var smtp = new System.Net.Mail.SmtpClient();
            {
                smtp.Host = ServerID;// "smtp.office365.com";
                smtp.Port = ServerPort;// 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                smtp.Timeout = 600000;
            }
            //Attchment
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(SenderIDAccount);

            foreach (var address in TOAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                mail.To.Add(address);
            }
            //mail.To.Add(TOAddress);
            foreach (var CCaddress in CcAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                mail.CC.Add(CCaddress);
            }
            mail.Subject = MailSubject;
            mail.Body = MailBody;
            if (txtAttachment1.Text != "")
            {
                System.Net.Mail.Attachment attachment1;
                attachment1 = new System.Net.Mail.Attachment(txtAttachment1.Text);
                mail.Attachments.Add(attachment1);
            }
            if (txtAttachment2.Text != "")
            {
                System.Net.Mail.Attachment attachment2;
                attachment2 = new System.Net.Mail.Attachment(txtAttachment2.Text);
                mail.Attachments.Add(attachment2);
            }
            if (txtAttachment3.Text != "")
            {
                System.Net.Mail.Attachment attachment3;
                attachment3 = new System.Net.Mail.Attachment(txtAttachment3.Text);
                mail.Attachments.Add(attachment3);
            }
            //sent without Certificate
            if (chkCertificate.Checked == true)
            {
                lblMsg.Text = "Try to send without certificate validation...[OK]";
                NEVER_EAT_POISON_Disable_CertificateValidation();
            }

            // Passing values to smtp object
            try
            {
                //   smtp.Send(SenderIDAccount, TOAddress, MailSubject, MailBody);
                smtp.Send(mail);
                //MessageBox.Show("eMail Sent Successfully...", "ASEM");
                lblMsg.Text = "eMail Sent Successfully... [OK]";
            }
            catch (Exception ex)
            {
                lblMsg.Text = "Getting error...[OK]";
                MessageBox.Show(ex.ToString());
            }
        }
        private void txtAttachment1_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            // choofdlog.Multiselect = true;
            if (choofdlog.ShowDialog() == DialogResult.OK)
                txtAttachment1.Text = choofdlog.FileName;
            else
                txtAttachment1.Text = string.Empty;
        }
        private void txtAttachment2_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            if (choofdlog.ShowDialog() == DialogResult.OK)
                txtAttachment2.Text = choofdlog.FileName;
            else
                txtAttachment2.Text = string.Empty;
        }
        private void txtAttachment3_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            if (choofdlog.ShowDialog() == DialogResult.OK)
                txtAttachment3.Text = choofdlog.FileName;
            else
                txtAttachment3.Text = string.Empty;
        }

        private void lblMsg_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }

        private void chkDefaulProxy_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDefaulProxy.Checked)
            {
                panel1.Enabled = false;
            }
            else {
                panel1.Enabled = true;
            }
        }

        private void btnNewMail_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            txtTO.Text = "";
            txtCC.Text = "";
            txtSubject.Text = "";
            txtAttachment1.Text = "";
            txtAttachment2.Text = "";
            txtAttachment3.Text = "";
            txtMailBody.Text = "";
            txtSubject.Focus();
        }

        private void SetProx()
        {
            ProxyIP = txtProxyServer.Text.Trim();
            ProxyPort = Convert.ToInt32(txtProxyPort.Text.Trim());
            ProxyUser = txtProxyUser.Text.Trim();
            ProxyPassword = txtProxyPassword.Text.Trim();
            IWebProxy proxy = new WebProxy(ProxyIP, ProxyPort)
            {
                Credentials = new NetworkCredential(ProxyUser, ProxyPassword)
            };
            //https://stackoverflow.com/questions/45079452/set-ip-and-port-in-proxy-in-c-sharp
            //set manual proxy in c# application site:stackoverflow.com
        }

    }
}
